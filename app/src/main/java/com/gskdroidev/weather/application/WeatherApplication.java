package com.gskdroidev.weather.application;


import android.app.Application;

import com.google.android.gms.location.FusedLocationProviderClient;

import com.gskdroidev.weather.dagger.components.DaggerApplicationComponent;
import com.gskdroidev.weather.dagger.components.ApplicationComponent;
import com.gskdroidev.weather.dagger.modules.ContextModule;

public class WeatherApplication extends Application {


    private final String TAG = this.getClass().getSimpleName();

    private static WeatherApplication application;

    private ApplicationComponent component;

    private FusedLocationProviderClient mFusedLocationClient;

    public static WeatherApplication getApplication() {
        return application;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        application = this;

        component = DaggerApplicationComponent.builder()
                .contextModule(new ContextModule(this))
                .build();


    }

    public ApplicationComponent getApplicationComponent(){
        return component;
    }

}
