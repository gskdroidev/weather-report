package com.gskdroidev.weather.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.Log;

import com.gskdroidev.weather.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BindingUtils {

    private static char degree = '\u00b0';

    public static String convertDegree(@NonNull Float temperature) {
        return temperature != null ? String.valueOf(Math.round(temperature)) + degree + "c" : "";
    }

    public static String concatString(String one, String two) {
        return one != null && two != null ? one + ", " + two : "";
    }

    public static Drawable getWeatherIcon(Context context, String code) {
        switch (code) {
            case "a01d":
                return context.getResources().getDrawable(R.drawable.a01d);
            case "a01n":
                return context.getResources().getDrawable(R.drawable.a01n);
            case "a02d":
                return context.getResources().getDrawable(R.drawable.a02d);
            case "a02n":
                return context.getResources().getDrawable(R.drawable.a02n);
            case "a03d":
                return context.getResources().getDrawable(R.drawable.a03d);
            case "a03n":
                return context.getResources().getDrawable(R.drawable.a03n);
            case "a04d":
                return context.getResources().getDrawable(R.drawable.a04d);
            case "a04n":
                return context.getResources().getDrawable(R.drawable.a04n);
            case "a05d":
                return context.getResources().getDrawable(R.drawable.a05d);
            case "a05n":
                return context.getResources().getDrawable(R.drawable.a05n);
            case "c01d":
                return context.getResources().getDrawable(R.drawable.c01d);
            case "c01n":
                return context.getResources().getDrawable(R.drawable.c01n);
            case "c02d":
                return context.getResources().getDrawable(R.drawable.c02d);
            case "c02n":
                return context.getResources().getDrawable(R.drawable.c02n);
            case "c03d":
                return context.getResources().getDrawable(R.drawable.c03d);
            case "c03n":
                return context.getResources().getDrawable(R.drawable.c03n);
            case "c04d":
                return context.getResources().getDrawable(R.drawable.c04d);
            case "c04n":
                return context.getResources().getDrawable(R.drawable.c04n);
            case "d01d":
                return context.getResources().getDrawable(R.drawable.d01d);
            case "d01n":
                return context.getResources().getDrawable(R.drawable.d01n);
            case "d02d":
                return context.getResources().getDrawable(R.drawable.d02d);
            case "d02n":
                return context.getResources().getDrawable(R.drawable.d02n);
            case "d03d":
                return context.getResources().getDrawable(R.drawable.d03d);
            case "d03n":
                return context.getResources().getDrawable(R.drawable.d03n);
            case "f01d":
                return context.getResources().getDrawable(R.drawable.f01d);
            case "f01n":
                return context.getResources().getDrawable(R.drawable.f01n);
            case "r01d":
                return context.getResources().getDrawable(R.drawable.r01d);
            case "r01n":
                return context.getResources().getDrawable(R.drawable.r01n);
            case "r02d":
                return context.getResources().getDrawable(R.drawable.r02d);
            case "r02n":
                return context.getResources().getDrawable(R.drawable.r02n);
            case "r03d":
                return context.getResources().getDrawable(R.drawable.r03d);
            case "r03n":
                return context.getResources().getDrawable(R.drawable.r03n);
            case "r04d":
                return context.getResources().getDrawable(R.drawable.r04d);
            case "r04n":
                return context.getResources().getDrawable(R.drawable.r04n);
            case "r05d":
                return context.getResources().getDrawable(R.drawable.r05d);
            case "r05n":
                return context.getResources().getDrawable(R.drawable.r05n);
            case "r06d":
                return context.getResources().getDrawable(R.drawable.r06d);
            case "r06n":
                return context.getResources().getDrawable(R.drawable.r06n);
            case "t01d":
                return context.getResources().getDrawable(R.drawable.t01d);
            case "t01n":
                return context.getResources().getDrawable(R.drawable.t01n);
            case "t02d":
                return context.getResources().getDrawable(R.drawable.t02d);
            case "t02n":
                return context.getResources().getDrawable(R.drawable.t02n);
            case "t03d":
                return context.getResources().getDrawable(R.drawable.t03d);
            case "t03n":
                return context.getResources().getDrawable(R.drawable.t03n);
            case "t04d":
                return context.getResources().getDrawable(R.drawable.t04d);
            case "t04n":
                return context.getResources().getDrawable(R.drawable.t04n);
            case "t05d":
                return context.getResources().getDrawable(R.drawable.t05d);
            case "t05n":
                return context.getResources().getDrawable(R.drawable.t05n);
            default:
                return context.getResources().getDrawable(R.drawable.c01d);

        }
    }


    public static int getBackgroundImage() {
        String time = new SimpleDateFormat("kk").format(new Date().getTime());

        Log.d("zzz", "Time: " + time);

        int currentTime = Integer.valueOf(time);
        if (currentTime >= 6 && currentTime <= 10) {
            return R.drawable.morning_sky;
        } else if (currentTime >= 11 && currentTime <= 16) {
            return R.drawable.noon_sky;
        } else if (currentTime>=17 && currentTime <= 19) {
            return R.drawable.evening_sky;
        } else {
            return R.drawable.night_sky;
        }

    }
}
