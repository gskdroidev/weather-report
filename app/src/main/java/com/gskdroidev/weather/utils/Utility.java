package com.gskdroidev.weather.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.ActivityCompat;

import com.gskdroidev.weather.model.pojo.daily_weather.DailyDatum;
import com.gskdroidev.weather.model.pojo.daily_weather.DailyWeather;
import com.gskdroidev.weather.model.pojo.hourly_weather.HourlyDatum;
import com.gskdroidev.weather.model.pojo.hourly_weather.HourlyWeather;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Utility {

    public static boolean checkInternetConnection(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    public static boolean checkLocationPermission(Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }


    public static List<HourlyDatum> getPresentHourlyData(HourlyWeather hourlyWeather) {

        List<HourlyDatum> localData = new ArrayList<>();

        for (int i = 0; i < hourlyWeather.getData().size(); i++) {
            HourlyDatum hourlyDatum = hourlyWeather.getData().get(i);

            long postTime = 0;
            @SuppressLint("SimpleDateFormat") SimpleDateFormat f = new SimpleDateFormat("yyy-MM-dd:HH");
            try {
                Date d = f.parse(hourlyDatum.getDatetime());
                postTime = d.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long currentTime = new Date().getTime();

            if (currentTime < postTime)
                localData.add(hourlyDatum);
        }

        return localData;

    }

    public static List<DailyDatum> getPresentDailyData(DailyWeather dailyWeather){

        List<DailyDatum> localDailyData = new ArrayList<>();

        for (int i = 0; i < dailyWeather.getData().size(); i++) {
            DailyDatum datum = dailyWeather.getData().get(i);

            int postTime = 0;
            @SuppressLint("SimpleDateFormat") SimpleDateFormat f = new SimpleDateFormat("yyy-MM-dd");
            try {
                Date d = f.parse(datum.getDatetime());
                postTime = d.getDate();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            int currentTime = new Date().getDate();

            if (currentTime <= postTime)
                localDailyData.add(datum);
        }

        return localDailyData;
    }


}
