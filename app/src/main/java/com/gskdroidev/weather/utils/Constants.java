package com.gskdroidev.weather.utils;

public class Constants {

    public static final String API_KEY = "4edc3de5a4a0445cb4178a791312ea57";

    public static final String LAST_SEARCHED_CITY = "last_search_city";

    public static final int LOCATION_PERMISSION_REQUEST = 404;
}
