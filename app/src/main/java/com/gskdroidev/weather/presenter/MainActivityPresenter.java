package com.gskdroidev.weather.presenter;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.gskdroidev.weather.model.interfaces.MainActivityView;
import com.gskdroidev.weather.model.interfaces.WeatherRepository;
import com.gskdroidev.weather.model.pojo.daily_weather.DailyWeather;
import com.gskdroidev.weather.model.pojo.hourly_weather.HourlyWeather;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public class MainActivityPresenter {

    private final String TAG = this.getClass().getSimpleName();

    private MainActivityView view;
    private WeatherRepository repository;

    public MainActivityPresenter(MainActivityView view, WeatherRepository repository) {
        this.view = view;
        this.repository = repository;
    }


    public void loadHourlyWeather(String city){
        repository.getHourlyWeather(city).subscribeWith(new SingleObserver<HourlyWeather>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(HourlyWeather hourlyWeather) {
               view.successHourlyWeather(hourlyWeather);
            }

            @Override
            public void onError(Throwable e) {
               view.failureHourlyWeather(e.getMessage());
            }
        });
    }

    public void loadHourlyWeatherWithLocation(Location location){
        repository.getHourlyWeatherWithLocation(location.getLatitude(),location.getLongitude()).subscribeWith(new SingleObserver<HourlyWeather>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(HourlyWeather hourlyWeather) {
                view.successHourlyWeather(hourlyWeather);
            }

            @Override
            public void onError(Throwable e) {
                view.failureHourlyWeather(e.getMessage());
            }
        });
    }

    public void loadDailyWeather(String city){
        repository.getDailyWeather(city).subscribeWith(new SingleObserver<DailyWeather>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(DailyWeather dailyWeather) {
                view.successDailyWeather(dailyWeather);
            }

            @Override
            public void onError(Throwable e) {
                view.failureDailyWeather(e.getMessage());
            }
        });
    }

    public void loadDailyWeatherWithLocation(Location location){
        repository.getDailyWeatherWithLocation(location.getLatitude(),location.getLongitude()).subscribeWith(new SingleObserver<DailyWeather>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(DailyWeather dailyWeather) {
                view.successDailyWeather(dailyWeather);
            }

            @Override
            public void onError(Throwable e) {
                view.failureDailyWeather(e.getMessage());
            }
        });
    }
}
