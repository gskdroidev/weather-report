package com.gskdroidev.weather.view.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.Task;
import com.gskdroidev.weather.R;
import com.gskdroidev.weather.application.WeatherApplication;
import com.gskdroidev.weather.dagger.components.DaggerMainActivityComponent;
import com.gskdroidev.weather.dagger.components.MainActivityComponent;
import com.gskdroidev.weather.dagger.modules.MainActivityModule;
import com.gskdroidev.weather.databinding.ActivityMainBinding;
import com.gskdroidev.weather.model.adapters.DailyWeatherAdapter;
import com.gskdroidev.weather.model.adapters.HourlyWeatherAdapter;
import com.gskdroidev.weather.model.interfaces.MainActivityView;
import com.gskdroidev.weather.model.pojo.daily_weather.DailyDatum;
import com.gskdroidev.weather.model.pojo.daily_weather.DailyWeather;
import com.gskdroidev.weather.model.pojo.hourly_weather.HourlyDatum;
import com.gskdroidev.weather.model.pojo.hourly_weather.HourlyWeather;
import com.gskdroidev.weather.presenter.MainActivityPresenter;
import com.gskdroidev.weather.utils.BindingUtils;
import com.gskdroidev.weather.utils.Constants;
import com.gskdroidev.weather.utils.Utility;
import com.gskdroidev.weather.view.fragments.DailyWeatherFragment;
import com.gskdroidev.weather.view.fragments.HourlyWeatherFragment;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.gskdroidev.weather.utils.Constants.LAST_SEARCHED_CITY;
import static com.gskdroidev.weather.utils.Utility.checkInternetConnection;
import static com.gskdroidev.weather.utils.Utility.checkLocationPermission;
import static com.gskdroidev.weather.utils.Utility.getPresentDailyData;


public class MainActivity extends BaseActivity implements MainActivityView, SearchView.OnQueryTextListener, HourlyWeatherFragment.OnHourlyWeatherSelectedListener, DailyWeatherFragment.OnDailyWeatherSelectedListener {

    private static final int LOCATION_SETTINGS_REQUEST = 101;
    private final String TAG = this.getClass().getSimpleName();


    @BindView(R.id.tool_bar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.navigation)
    NavigationView mNavigationView;

    @BindView(R.id.main_container)
    ConstraintLayout constraintLayoutMainContainer;

    @BindView(R.id.weather_image)
    ImageView imageViewWeatherIcon;


    @BindView(R.id.hourly_container)
    ConstraintLayout constraintLayoutHourlyContainer;
    @BindView(R.id.current_time)
    TextView textViewCurrentTime;
    @BindView(R.id.hourly_weather_report)
    RecyclerView recyclerViewHourlyWeatherReport;


    @BindView(R.id.daily_container)
    ConstraintLayout constraintLayoutDailyContainer;
    @BindView(R.id.current_date)
    TextView textViewCurrentDate;
    @BindView(R.id.daily_weather_report)
    RecyclerView recyclerViewDailyWeatherReport;


    @Inject
    FusedLocationProviderClient fusedLocationProviderClient;

    @Inject
    MainActivityPresenter presenter;


    @BindView(R.id.fragment_container)
    LinearLayout linearLayoutFragmentContainer;

    ActivityMainBinding activityMainBinding;
    LocationManager locationManager;
    LocationListener locationListener;
    Location location;
    SharedPreferences sharedPreferences;

    HourlyWeather hourlyWeather;
    DailyWeather dailyWeather;

    FragmentManager fragmentManager;

    private SearchView mSearchView;
    private ActionBarDrawerToggle mDrawerToggle;
    private HourlyWeatherAdapter hourlyWeatherAdapter;
    private DailyWeatherAdapter dailyWeatherAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        ButterKnife.bind(this);

        MainActivityComponent component = DaggerMainActivityComponent.builder()
                .applicationComponent(WeatherApplication.getApplication().getApplicationComponent())
                .mainActivityModule(new MainActivityModule(this))
                .build();

        component.injectMainActivity(this);

        setSupportActionBar(activityMainBinding.toolBar);

        setTitle("");

        init();

    }


    private void init() {

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);


        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                Log.d(TAG, "Listener location: " + location.getLatitude());
                MainActivity.this.location = location;
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        String lastSearchedCity = sharedPreferences.getString(LAST_SEARCHED_CITY, null);

        if (lastSearchedCity != null) {
            searchCity(lastSearchedCity);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        mNavigationView.setNavigationItemSelectedListener(menuItem -> {

            mDrawerLayout.closeDrawers();
            menuItem.setChecked(true);
            switch (menuItem.getItemId()) {

            }
            return true;
        });

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        mDrawerToggle.setDrawerIndicatorEnabled(false);

        mDrawerToggle.setToolbarNavigationClickListener(view -> mDrawerLayout.openDrawer(GravityCompat.START));

        mDrawerToggle.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
    }


    @Override
    protected void onStart() {
        super.onStart();
        constraintLayoutMainContainer.setBackground(getResources().getDrawable(BindingUtils.getBackgroundImage()));
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (dailyWeather != null && hourlyWeather != null) {
            updateDailyWeather();
            updateHourlyWeather();
        }
    }

    @Override
    public void successHourlyWeather(HourlyWeather hourlyWeather) {
        Log.d(TAG, "successHourlyWeather success: ");
        this.hourlyWeather = hourlyWeather;
        sharedPreferences.edit().putString(LAST_SEARCHED_CITY, hourlyWeather.getCityName()).apply();
        updateHourlyWeather();
    }

    @Override
    public void failureHourlyWeather(String message) {
        Log.d(TAG, "failureHourlyWeather Error: " + message);
        runOnUiThread(() -> Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show());
    }

    @Override
    public void successDailyWeather(DailyWeather dailyWeather) {
        this.dailyWeather = dailyWeather;

        updateDailyWeather();
    }


    @Override
    public void failureDailyWeather(String message) {
        Log.d(TAG, "failureDailyWeather: " + message);
    }


    private void updateHourlyWeather() {

        List<HourlyDatum> currentHourlyWeather = Utility.getPresentHourlyData(hourlyWeather);

        activityMainBinding.setWeather(hourlyWeather);
        activityMainBinding.setData(currentHourlyWeather.get(0));

        setHeaderHourlyWeather(currentHourlyWeather.get(0));
        setHourlyWeatherAdapter(currentHourlyWeather);
    }

    private void updateDailyWeather() {
        List<DailyDatum> dailyData = getPresentDailyData(dailyWeather);
        setDailyWeatherHeader(dailyData.get(0));
        setDailyWeatherAdapter(dailyData);
    }

    private void setHeaderHourlyWeather(HourlyDatum currentHourlyWeather) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat f = new SimpleDateFormat("yyy-MM-dd:HH");

        try {
            Date date = f.parse(currentHourlyWeather.getDatetime());
            @SuppressLint("SimpleDateFormat") String now = new SimpleDateFormat("h:mm aa").format(date);

            runOnUiThread(() -> {
                textViewCurrentTime.setText(now);
                imageViewWeatherIcon.setImageDrawable(BindingUtils.getWeatherIcon(MainActivity.this
                        , currentHourlyWeather.getWeather().getIcon()));
            });

        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    private void setHourlyWeatherAdapter(List<HourlyDatum> currentHourlyWeather) {

        if (currentHourlyWeather.size() > 0) {
            runOnUiThread(() -> {
                if (currentHourlyWeather.size() > 6) {
                    hourlyWeatherAdapter = new HourlyWeatherAdapter(MainActivity.this, currentHourlyWeather.subList(0, 5), false);
                } else {
                    hourlyWeatherAdapter = new HourlyWeatherAdapter(MainActivity.this, currentHourlyWeather, false);
                }

                recyclerViewHourlyWeatherReport.setLayoutManager(new GridLayoutManager(MainActivity.this, hourlyWeatherAdapter.getItemCount()));
                recyclerViewHourlyWeatherReport.setAdapter(hourlyWeatherAdapter);

                constraintLayoutHourlyContainer.setVisibility(View.VISIBLE);
                constraintLayoutHourlyContainer.setOnClickListener(view -> {
                    linearLayoutFragmentContainer.setVisibility(View.VISIBLE);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("data", (Serializable) currentHourlyWeather);

                    HourlyWeatherFragment hourlyWeatherFragment = new HourlyWeatherFragment();
                    hourlyWeatherFragment.setArguments(bundle);

                    setFragment(hourlyWeatherFragment);
                });
            });

        }
    }

    @SuppressLint("SimpleDateFormat")
    private void setDailyWeatherHeader(DailyDatum dailyDatum) {

        SimpleDateFormat f = new SimpleDateFormat("yyy-MM-dd");
        try {
            Date d = f.parse(dailyDatum.getDatetime());
            f = new SimpleDateFormat("dd-MMM");
            String date = f.format(d);

            runOnUiThread(() -> {
                textViewCurrentDate.setText(date);
            });

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private void setDailyWeatherAdapter(List<DailyDatum> dailyData) {
        if (dailyData.size() > 0) {
            runOnUiThread(() -> {
                if (dailyData.size() > 6) {
                    dailyWeatherAdapter = new DailyWeatherAdapter(MainActivity.this, dailyData.subList(0, 5), false);
                } else {
                    dailyWeatherAdapter = new DailyWeatherAdapter(MainActivity.this, dailyData, false);
                }

                recyclerViewDailyWeatherReport.setLayoutManager(new GridLayoutManager(MainActivity.this, dailyWeatherAdapter.getItemCount()));
                recyclerViewDailyWeatherReport.setAdapter(dailyWeatherAdapter);

                constraintLayoutDailyContainer.setVisibility(View.VISIBLE);
                constraintLayoutDailyContainer.setOnClickListener(view -> {
                    linearLayoutFragmentContainer.setVisibility(View.VISIBLE);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("data", (Serializable) dailyData);

                    DailyWeatherFragment dailyWeatherFragment = new DailyWeatherFragment();
                    dailyWeatherFragment.setArguments(bundle);

                    setFragment(dailyWeatherFragment);
                });
            });

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        MenuItem search = menu.findItem(R.id.search);
        mSearchView = (SearchView) search.getActionView();
        mSearchView.setQueryHint(getResources().getString(R.string.search_hint));
        mSearchView.setIconifiedByDefault(true);
        mSearchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.location:
                toolbar.collapseActionView();
                searchWeatherByLocation();
                return true;
            default:
                return false;
        }
    }


    private void searchWeatherByLocation() {
        if (checkLocationPermission(this)) {
            checkLocationStatus();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST);
        }
    }


    public void checkLocationStatus() {
        LocationRequest mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setMaxWaitTime(60 * 60 * 1000)
                .setInterval(10 * 60 * 1000);


        LocationSettingsRequest.Builder settingsBuilder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        settingsBuilder.setAlwaysShow(true);

        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(this)
                .checkLocationSettings(settingsBuilder.build());


        result.addOnCompleteListener(task -> {
            try {
                LocationSettingsResponse response = task.getResult(ApiException.class);
                Log.d(TAG, "Location status: " + response.getLocationSettingsStates().isLocationPresent());
                initialLocationListener();
                getGpsLocation();

            } catch (ApiException ex) {
                switch (ex.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            ResolvableApiException resolvableApiException = (ResolvableApiException) ex;
                            resolvableApiException.startResolutionForResult(MainActivity.this, LOCATION_SETTINGS_REQUEST);
                        } catch (IntentSender.SendIntentException e) {
                            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        break;
                }
            }

        });
    }


    private void stopLocationListener() {
        locationManager.removeUpdates(locationListener);
    }

    private void initialLocationListener() {// Acquire a reference to the system Location Manager

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
    }

    private void getGpsLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (location != null) {
                getAddressFromLocation(location);
                stopLocationListener();
            } else {
                fusedLocationProviderClient.getLastLocation().addOnSuccessListener(location -> {
                    if (location != null) {
                        getAddressFromLocation(location);
                        stopLocationListener();
                    } else {
                        getGpsLocation();
                    }
                }).addOnFailureListener(e -> Log.d(TAG, "Location failure: " + e.getMessage()));
            }
        }

    }


    private void getAddressFromLocation(Location location) {

        if (Utility.checkInternetConnection(this)) {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                presenter.loadHourlyWeather(addresses.get(0).getLocality());
                presenter.loadDailyWeather(addresses.get(0).getLocality());
            } catch (IOException e) {
                presenter.loadHourlyWeatherWithLocation(location);
                presenter.loadDailyWeatherWithLocation(location);
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case Constants.LOCATION_PERMISSION_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    searchWeatherByLocation();
                } else {
                    Toast.makeText(this, "Permission denied!!", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case LOCATION_SETTINGS_REQUEST:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        initialLocationListener();
                        getGpsLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(this, "Location on denied!!!", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
                break;
        }
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        toolbar.collapseActionView();
        searchCity(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    private void setFragment(Fragment fragment) {
        fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
    }

    private void searchCity(String city) {
        if (checkInternetConnection(this)) {
            presenter.loadHourlyWeather(city);
            presenter.loadDailyWeather(city);
        } else {
            Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void closeFragment() {
        fragmentManager.beginTransaction().remove(fragmentManager.findFragmentById(R.id.fragment_container)).commit();
    }
}
