package com.gskdroidev.weather.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gskdroidev.weather.R;
import com.gskdroidev.weather.model.adapters.DailyWeatherAdapter;
import com.gskdroidev.weather.model.adapters.HourlyWeatherAdapter;
import com.gskdroidev.weather.model.pojo.daily_weather.DailyDatum;
import com.gskdroidev.weather.model.pojo.hourly_weather.HourlyDatum;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DailyWeatherFragment extends Fragment {

    @BindView(R.id.title)
    TextView textViewTitle;
    @BindView(R.id.fragment_close)
    ImageView imageViewFragmentClose;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private OnDailyWeatherSelectedListener callback;

    public interface OnDailyWeatherSelectedListener {
        void closeFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnDailyWeatherSelectedListener) {
            callback = (OnDailyWeatherSelectedListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnHourlyWeatherSelectedListener");
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragment = inflater.inflate(R.layout.fragment_daily_weather, container, false);
        ButterKnife.bind(this, fragment);
        return fragment;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayList<DailyDatum> strtext = (ArrayList<DailyDatum>) getArguments().getSerializable("data");

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new DailyWeatherAdapter(getActivity(), strtext, true));

        textViewTitle.setText(getResources().getString(R.string.daily_weather));

        imageViewFragmentClose.setOnClickListener(v -> {
            Log.d("zzz", "Close fragment");
            callback.closeFragment();
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

}
