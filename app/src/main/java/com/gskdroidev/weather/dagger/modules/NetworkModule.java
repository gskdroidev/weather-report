package com.gskdroidev.weather.dagger.modules;

import android.content.Context;

import com.gskdroidev.weather.dagger.scopes.ApplicationScope;
import com.gskdroidev.weather.utils.Constants;

import java.io.File;


import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = ContextModule.class)
public class NetworkModule {

    @Provides
    @ApplicationScope
    public Retrofit retrofit(OkHttpClient okHttpClient) {

        return new Retrofit.Builder()
                .baseUrl("https://api.weatherbit.io/v2.0/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }


    @Provides
    @ApplicationScope
    public OkHttpClient okHttpClient(Cache cache) {

        return new OkHttpClient().newBuilder()
                .addInterceptor(chain -> {
                    Request request = chain.request();

                    HttpUrl httpUrl = request.url();

                    HttpUrl newHttpUrl = httpUrl.newBuilder()
                            .addQueryParameter("key", Constants.API_KEY)
                            .build();

                    Request.Builder requestBuilder = request.newBuilder()
                            .url(newHttpUrl);

                    Request newRequest = requestBuilder.build();

                    return chain.proceed(newRequest);
                })
                .addNetworkInterceptor(chain -> {
                    Request request = chain.request();
                    Response response = chain.proceed(request);

                    int maxAge = 60 * 60 * 36;

                    return response.newBuilder()
                            .header("Cache-Control", "public, max-age=" + maxAge)
                            .build();
                })
                .cache(cache)
                .build();
    }


    @Provides
    @ApplicationScope
    public Cache cache(File file) {
        return new Cache(file, 10 * 1024 * 1024);
    }

    @Provides
    @ApplicationScope
    public File file(Context context) {
        return new File(context.getCacheDir(), "weather_cache");
    }


}
