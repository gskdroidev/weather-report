package com.gskdroidev.weather.dagger.modules;

import android.content.Context;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.gskdroidev.weather.dagger.scopes.ApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module(includes = ContextModule.class)
public class LocationModule {

    @Provides
    @ApplicationScope
    public FusedLocationProviderClient locationClient(Context context){
        return LocationServices.getFusedLocationProviderClient(context);
    }

}
