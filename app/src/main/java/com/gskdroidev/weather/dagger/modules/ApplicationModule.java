package com.gskdroidev.weather.dagger.modules;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.gskdroidev.weather.dagger.scopes.ApplicationScope;
import com.gskdroidev.weather.dagger.scopes.MainActivityScope;
import com.gskdroidev.weather.model.retrofit.WeatherService;


import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@ApplicationScope
@Module(includes = {NetworkModule.class,ContextModule.class})
public class ApplicationModule {

    @Provides
    @ApplicationScope
    public WeatherService getWeatherService(Retrofit retrofit) {
        return retrofit.create(WeatherService.class);
    }

    @Provides
    @ApplicationScope
    SharedPreferences sharedPreferences(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}
