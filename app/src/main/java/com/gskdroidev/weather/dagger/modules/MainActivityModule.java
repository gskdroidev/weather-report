package com.gskdroidev.weather.dagger.modules;


import com.gskdroidev.weather.dagger.scopes.MainActivityScope;
import com.gskdroidev.weather.model.interfaces.MainActivityView;
import com.gskdroidev.weather.model.interfaces.WeatherRepository;
import com.gskdroidev.weather.model.repositories.NetworkWeatherRepository;
import com.gskdroidev.weather.presenter.MainActivityPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {

    private MainActivityView mainActivityView;

    public MainActivityModule(MainActivityView mainActivityView){
        this.mainActivityView = mainActivityView;
    }

    @Provides
    @MainActivityScope
    MainActivityPresenter mainActivityPresenter(MainActivityView mainActivityView, WeatherRepository weatherRepository) {
        return new MainActivityPresenter(mainActivityView, weatherRepository);
    }

    @Provides
    @MainActivityScope
    MainActivityView mainActivityView(){
        return mainActivityView;
    }

    @Provides
    @MainActivityScope
    WeatherRepository weatherRepository(){
        return new NetworkWeatherRepository();
    }


}
