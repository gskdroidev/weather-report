package com.gskdroidev.weather.dagger.components;


import com.google.android.gms.location.FusedLocationProviderClient;
import com.gskdroidev.weather.dagger.modules.LocationModule;
import com.gskdroidev.weather.dagger.scopes.ApplicationScope;
import com.gskdroidev.weather.dagger.modules.ApplicationModule;
import com.gskdroidev.weather.model.retrofit.WeatherService;

import dagger.Component;


@ApplicationScope
@Component(modules = {ApplicationModule.class, LocationModule.class})
public interface ApplicationComponent {

    WeatherService getWeatherService();

    FusedLocationProviderClient getLocationService();
}
