package com.gskdroidev.weather.dagger.components;

import com.gskdroidev.weather.dagger.modules.MainActivityModule;
import com.gskdroidev.weather.dagger.scopes.MainActivityScope;
import com.gskdroidev.weather.view.activity.MainActivity;

import dagger.Component;

@Component(modules = MainActivityModule.class, dependencies = ApplicationComponent.class)
@MainActivityScope
public interface MainActivityComponent {

    void injectMainActivity(MainActivity mainActivity);
}
