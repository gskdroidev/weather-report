package com.gskdroidev.weather.model.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gskdroidev.weather.R;
import com.gskdroidev.weather.model.pojo.daily_weather.DailyDatum;
import com.gskdroidev.weather.utils.BindingUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DailyWeatherAdapter extends RecyclerView.Adapter {

    private Context context;
    private LayoutInflater inflater;
    private List<DailyDatum> data;
    private boolean isListView;
    private DailyWeatherListViewHolder dailyWeatherListViewHolder;
    private DailyWeatherViewHolder dailyWeatherViewHolder;

    public DailyWeatherAdapter(Context context, List<DailyDatum> data, boolean isListView) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.data = data;
        this.isListView = isListView;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (isListView) {
            View view = inflater.inflate(R.layout.adapter_daily_weather_list_item, parent, false);
            return new DailyWeatherListViewHolder(view);
        } else {
            View view = inflater.inflate(R.layout.adapter_daily_weather_item, parent, false);
            return new DailyWeatherViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        DailyDatum dailyDatum = data.get(holder.getAdapterPosition());

        if (isListView) {
            dailyWeatherListViewHolder = (DailyWeatherListViewHolder) holder;
            dailyWeatherListViewHolder.bindView(dailyDatum);
        } else {
            dailyWeatherViewHolder = (DailyWeatherViewHolder) holder;
            dailyWeatherViewHolder.bindView(dailyDatum);
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    class DailyWeatherViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.day)
        TextView textViewTime;
        @BindView(R.id.max_temperature)
        TextView textViewMaxTemperature;
        @BindView(R.id.min_temperature)
        TextView textViewMinTemperature;
        @BindView(R.id.icon)
        ImageView imageViewIcon;

        private DailyWeatherViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindView(DailyDatum dailyDatum) {
            imageViewIcon.setImageDrawable(BindingUtils.getWeatherIcon(context, dailyDatum.getWeather().getIcon()));

            @SuppressLint("SimpleDateFormat") SimpleDateFormat f = new SimpleDateFormat("yyy-MM-dd");
            try {
                Date d = f.parse(dailyDatum.getDatetime());
                f = new SimpleDateFormat("E");
                textViewTime.setText(f.format(d));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            textViewMaxTemperature.setText(BindingUtils.convertDegree(dailyDatum.getMaxTemp()));

            textViewMinTemperature.setText(BindingUtils.convertDegree(dailyDatum.getMinTemp()));
        }
    }

    class DailyWeatherListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.day)
        TextView textViewTime;
        @BindView(R.id.max_temperature)
        TextView textViewMaxTemperature;
        @BindView(R.id.min_temperature)
        TextView textViewMinTemperature;
        @BindView(R.id.icon)
        ImageView imageViewIcon;

        @BindView(R.id.sunrise)
        TextView textViewSunrise;
        @BindView(R.id.sunset)
        TextView textViewSunset;
        @BindView(R.id.date_month)
        TextView textViewDateAndMonth;
        @BindView(R.id.climate)
        TextView textViewClimate;
        @BindView(R.id.wind_direction_speed)
        TextView textViewWindDirection;

        DailyWeatherListViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
        }

        private void bindView(DailyDatum dailyDatum) {
            imageViewIcon.setImageDrawable(BindingUtils.getWeatherIcon(context, dailyDatum.getWeather().getIcon()));

            @SuppressLint("SimpleDateFormat") SimpleDateFormat f = new SimpleDateFormat("yyy-MM-dd");
            try {
                Date d = f.parse(dailyDatum.getDatetime());
                f = new SimpleDateFormat("EEEE");
                textViewTime.setText(f.format(d).toUpperCase());
                f = new SimpleDateFormat("MMMM dd");
                textViewDateAndMonth.setText(f.format(d).toUpperCase());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Date sunriseTime = new Date(TimeUnit.SECONDS.toMillis(dailyDatum.getSunriseTs()));
            String sunrise = new SimpleDateFormat("hh:mm aa").format(sunriseTime);
            textViewSunrise.setText(sunrise);

            Date sunsetTime = new Date(TimeUnit.SECONDS.toMillis(dailyDatum.getSunsetTs()));
            String sunset = new SimpleDateFormat("hh:mm aa").format(sunsetTime);
            textViewSunset.setText(String.valueOf(sunset));


            textViewMaxTemperature.setText(BindingUtils.convertDegree(dailyDatum.getMaxTemp()));
            textViewClimate.setText(dailyDatum.getWeather().getDescription());
            textViewWindDirection.setText(dailyDatum.getWindCdir() + dailyDatum.getWindSpd().intValue());

            textViewMinTemperature.setText(BindingUtils.convertDegree(dailyDatum.getMinTemp()));
        }
    }
}
