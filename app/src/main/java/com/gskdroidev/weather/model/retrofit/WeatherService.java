package com.gskdroidev.weather.model.retrofit;

import com.gskdroidev.weather.model.pojo.daily_weather.DailyWeather;
import com.gskdroidev.weather.model.pojo.hourly_weather.HourlyWeather;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherService {

    @GET("forecast/hourly")
    Single<HourlyWeather> getHourlyWeather(@Query("city") String city, @Query("hours") int hours);

   //https://api.weatherbit.io/v2.0/forecast/hourly?lat=38.123&lon=-78.543&key=4edc3de5a4a0445cb4178a791312ea57&hours=48
    @GET("forecast/hourly")
    Single<HourlyWeather> getHourlyWeatherWithLocation(@Query("lat") double lat, @Query("lon") double lon, @Query("hours") int hours);


    //https://api.weatherbit.io/v2.0/forecast/daily?city=Raleigh,NC&key=4edc3de5a4a0445cb4178a791312ea57
    @GET("forecast/daily")
    Single<DailyWeather> getDailyWeather(@Query("city") String city);

    @GET("forecast/daily")
    Single<DailyWeather> getDailyWeatherWithLocation(@Query("lat") double lat, @Query("lon") double lon);

}
