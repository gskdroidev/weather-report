package com.gskdroidev.weather.model.interfaces;


import com.gskdroidev.weather.model.pojo.daily_weather.DailyWeather;
import com.gskdroidev.weather.model.pojo.hourly_weather.HourlyWeather;

public interface MainActivityView {

    void successHourlyWeather(HourlyWeather hourlyWeather);
    void failureHourlyWeather(String message);

    void successDailyWeather(DailyWeather hourlyWeather);
    void failureDailyWeather(String message);

}
