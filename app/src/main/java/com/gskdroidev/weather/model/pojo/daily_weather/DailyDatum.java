
package com.gskdroidev.weather.model.pojo.daily_weather;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.gskdroidev.weather.model.pojo.Weather;

public class DailyDatum {

    @SerializedName("moonrise_ts")
    @Expose
    private Integer moonriseTs;
    @SerializedName("wind_cdir")
    @Expose
    private String windCdir;
    @SerializedName("rh")
    @Expose
    private Float rh;
    @SerializedName("pres")
    @Expose
    private Float pres;
    @SerializedName("sunset_ts")
    @Expose
    private Integer sunsetTs;
    @SerializedName("ozone")
    @Expose
    private Float ozone;
    @SerializedName("moon_phase")
    @Expose
    private Float moonPhase;
    @SerializedName("wind_gust_spd")
    @Expose
    private Float windGustSpd;
    @SerializedName("snow_depth")
    @Expose
    private Integer snowDepth;
    @SerializedName("clouds")
    @Expose
    private Float clouds;
    @SerializedName("ts")
    @Expose
    private Integer ts;
    @SerializedName("sunrise_ts")
    @Expose
    private Integer sunriseTs;
    @SerializedName("app_min_temp")
    @Expose
    private Float appMinTemp;
    @SerializedName("wind_spd")
    @Expose
    private Float windSpd;
    @SerializedName("pop")
    @Expose
    private Float pop;
    @SerializedName("wind_cdir_full")
    @Expose
    private String windCdirFull;
    @SerializedName("slp")
    @Expose
    private Float slp;
    @SerializedName("app_max_temp")
    @Expose
    private Float appMaxTemp;
    @SerializedName("vis")
    @Expose
    private Float vis;
    @SerializedName("dewpt")
    @Expose
    private Float dewpt;
    @SerializedName("snow")
    @Expose
    private Integer snow;
    @SerializedName("uv")
    @Expose
    private Float uv;
    @SerializedName("valid_date")
    @Expose
    private String validDate;
    @SerializedName("wind_dir")
    @Expose
    private Integer windDir;
    @SerializedName("max_dhi")
    @Expose
    private Object maxDhi;
    @SerializedName("clouds_hi")
    @Expose
    private Float cloudsHi;
    @SerializedName("precip")
    @Expose
    private Float precip;
    @SerializedName("weather")
    @Expose
    private Weather weather;
    @SerializedName("max_temp")
    @Expose
    private Float maxTemp;
    @SerializedName("moonset_ts")
    @Expose
    private Integer moonsetTs;
    @SerializedName("datetime")
    @Expose
    private String datetime;
    @SerializedName("temp")
    @Expose
    private Float temp;
    @SerializedName("min_temp")
    @Expose
    private Float minTemp;
    @SerializedName("clouds_mid")
    @Expose
    private Float cloudsMid;
    @SerializedName("clouds_low")
    @Expose
    private Float cloudsLow;

    public Integer getMoonriseTs() {
        return moonriseTs;
    }

    public void setMoonriseTs(Integer moonriseTs) {
        this.moonriseTs = moonriseTs;
    }

    public String getWindCdir() {
        return windCdir;
    }

    public void setWindCdir(String windCdir) {
        this.windCdir = windCdir;
    }

    public Float getRh() {
        return rh;
    }


    public Float getPres() {
        return pres;
    }

    public void setPres(Float pres) {
        this.pres = pres;
    }

    public Integer getSunsetTs() {
        return sunsetTs;
    }

    public void setSunsetTs(Integer sunsetTs) {
        this.sunsetTs = sunsetTs;
    }

    public Float getOzone() {
        return ozone;
    }

    public void setOzone(Float ozone) {
        this.ozone = ozone;
    }

    public Float getMoonPhase() {
        return moonPhase;
    }

    public void setMoonPhase(Float moonPhase) {
        this.moonPhase = moonPhase;
    }

    public Float getWindGustSpd() {
        return windGustSpd;
    }

    public void setWindGustSpd(Float windGustSpd) {
        this.windGustSpd = windGustSpd;
    }

    public Integer getSnowDepth() {
        return snowDepth;
    }

    public void setSnowDepth(Integer snowDepth) {
        this.snowDepth = snowDepth;
    }

    public Float getClouds() {
        return clouds;
    }


    public Integer getTs() {
        return ts;
    }

    public void setTs(Integer ts) {
        this.ts = ts;
    }

    public Integer getSunriseTs() {
        return sunriseTs;
    }

    public void setSunriseTs(Integer sunriseTs) {
        this.sunriseTs = sunriseTs;
    }

    public Float getAppMinTemp() {
        return appMinTemp;
    }

    public void setAppMinTemp(Float appMinTemp) {
        this.appMinTemp = appMinTemp;
    }

    public Float getWindSpd() {
        return windSpd;
    }

    public void setWindSpd(Float windSpd) {
        this.windSpd = windSpd;
    }

    public Float getPop() {
        return pop;
    }


    public String getWindCdirFull() {
        return windCdirFull;
    }

    public void setWindCdirFull(String windCdirFull) {
        this.windCdirFull = windCdirFull;
    }

    public Float getSlp() {
        return slp;
    }

    public void setSlp(Float slp) {
        this.slp = slp;
    }

    public Float getAppMaxTemp() {
        return appMaxTemp;
    }

    public void setAppMaxTemp(Float appMaxTemp) {
        this.appMaxTemp = appMaxTemp;
    }

    public Float getVis() {
        return vis;
    }

    public void setVis(Float vis) {
        this.vis = vis;
    }

    public Float getDewpt() {
        return dewpt;
    }

    public void setDewpt(Float dewpt) {
        this.dewpt = dewpt;
    }

    public Integer getSnow() {
        return snow;
    }

    public void setSnow(Integer snow) {
        this.snow = snow;
    }

    public Float getUv() {
        return uv;
    }

    public void setUv(Float uv) {
        this.uv = uv;
    }

    public String getValidDate() {
        return validDate;
    }

    public void setValidDate(String validDate) {
        this.validDate = validDate;
    }

    public Integer getWindDir() {
        return windDir;
    }

    public void setWindDir(Integer windDir) {
        this.windDir = windDir;
    }

    public Object getMaxDhi() {
        return maxDhi;
    }

    public void setMaxDhi(Object maxDhi) {
        this.maxDhi = maxDhi;
    }

    public Float getCloudsHi() {
        return cloudsHi;
    }

    public Float getPrecip() {
        return precip;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public Float getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(Float maxTemp) {
        this.maxTemp = maxTemp;
    }

    public Integer getMoonsetTs() {
        return moonsetTs;
    }

    public void setMoonsetTs(Integer moonsetTs) {
        this.moonsetTs = moonsetTs;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public Float getTemp() {
        return temp;
    }

    public void setTemp(Float temp) {
        this.temp = temp;
    }

    public Float getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(Float minTemp) {
        this.minTemp = minTemp;
    }

    public Float getCloudsMid() {
        return cloudsMid;
    }

    public Float getCloudsLow() {
        return cloudsLow;
    }


}
