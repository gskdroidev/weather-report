package com.gskdroidev.weather.model.repositories;

import android.util.Log;

import com.gskdroidev.weather.application.WeatherApplication;
import com.gskdroidev.weather.model.interfaces.WeatherRepository;
import com.gskdroidev.weather.model.pojo.daily_weather.DailyWeather;
import com.gskdroidev.weather.model.pojo.hourly_weather.HourlyWeather;


import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class NetworkWeatherRepository implements WeatherRepository {

    @Override
    public Single<HourlyWeather> getHourlyWeather(String city) {
        return WeatherApplication.getApplication().getApplicationComponent().getWeatherService()
                .getHourlyWeather(city,48)
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Single<HourlyWeather> getHourlyWeatherWithLocation(double latitude, double longitude) {
        Log.d("Main","Location : " + latitude + longitude);
        return WeatherApplication.getApplication().getApplicationComponent().getWeatherService()
                .getHourlyWeatherWithLocation(latitude,longitude,48)
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Single<DailyWeather> getDailyWeather(String city) {
        return WeatherApplication.getApplication().getApplicationComponent().getWeatherService()
                .getDailyWeather(city)
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Single<DailyWeather> getDailyWeatherWithLocation(double latitude, double longitude) {
        return WeatherApplication.getApplication().getApplicationComponent().getWeatherService()
                .getDailyWeatherWithLocation(latitude,longitude)
                .subscribeOn(Schedulers.io());
    }
}
