
package com.gskdroidev.weather.model.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Weather {

    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("description")
    @Expose
    private String description;

    public String getIcon() {
        return icon;
    }


    public String getCode() {
        return code;
    }


    public String getDescription() {
        return description;
    }


}
