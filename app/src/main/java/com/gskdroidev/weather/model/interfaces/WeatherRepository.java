package com.gskdroidev.weather.model.interfaces;


import com.gskdroidev.weather.model.pojo.daily_weather.DailyWeather;
import com.gskdroidev.weather.model.pojo.hourly_weather.HourlyWeather;

import io.reactivex.Single;

public interface WeatherRepository {

   Single<HourlyWeather> getHourlyWeather(String city);

   Single<HourlyWeather> getHourlyWeatherWithLocation(double latitude, double longitude);

   Single<DailyWeather> getDailyWeather(String city);

   Single<DailyWeather> getDailyWeatherWithLocation(double latitude, double longitude);

}
