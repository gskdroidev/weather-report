package com.gskdroidev.weather.model.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gskdroidev.weather.R;
import com.gskdroidev.weather.model.pojo.hourly_weather.HourlyDatum;
import com.gskdroidev.weather.utils.BindingUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HourlyWeatherAdapter extends RecyclerView.Adapter {

    private Context context;
    private LayoutInflater inflater;
    private List<HourlyDatum> data;
    private boolean isListView;
    private HourlyWeatherViewHolder hourlyWeatherViewHolder;
    private HourlyWeatherListViewHolder hourlyWeatherListViewHolder;

    public HourlyWeatherAdapter(Context context, List<HourlyDatum> data, boolean isListView) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.data = data;
        this.isListView = isListView;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (isListView) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_hourly_weather_list, parent, false);
            return new HourlyWeatherListViewHolder(view);
        } else {
            View view = inflater.inflate(R.layout.adapter_hourly_weather_item, parent, false);
            return new HourlyWeatherViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        HourlyDatum hourlyDatum = data.get(holder.getAdapterPosition());

        if (isListView) {
            hourlyWeatherListViewHolder = (HourlyWeatherListViewHolder) holder;
            hourlyWeatherListViewHolder.bindView(hourlyDatum);
        } else {
            hourlyWeatherViewHolder = (HourlyWeatherViewHolder) holder;
            hourlyWeatherViewHolder.bindView(hourlyDatum);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    class HourlyWeatherViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.time)
        TextView textViewTime;

        @BindView(R.id.temperature)
        TextView textViewTemperature;

        @BindView(R.id.icon)
        ImageView imageViewIcon;

        private HourlyWeatherViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindView(HourlyDatum hourlyDatum) {
            Date date = null;
            @SuppressLint("SimpleDateFormat") SimpleDateFormat f = new SimpleDateFormat("yyy-MM-dd:HH");
            try {
                date = f.parse(hourlyDatum.getDatetime());
                @SuppressLint("SimpleDateFormat") String now = new SimpleDateFormat("h aa").format(date);
                textViewTime.setText(now);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            textViewTemperature.setText(BindingUtils.convertDegree(hourlyDatum.getAppTemp()));
            imageViewIcon.setImageDrawable(BindingUtils.getWeatherIcon(context, hourlyDatum.getWeather().getIcon()));
        }
    }

    class HourlyWeatherListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.time)
        TextView textViewTime;
        @BindView(R.id.icon)
        ImageView imageViewWeatherIcon;
        @BindView(R.id.temperature)
        TextView textViewWeatherTemperature;
        @BindView(R.id.wind_direction)
        TextView textViewWindDirection;

        HourlyWeatherListViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
        }

        private void bindView(HourlyDatum hourlyDatum) {
            Date date = null;
            @SuppressLint("SimpleDateFormat") SimpleDateFormat f = new SimpleDateFormat("yyy-MM-dd:HH");
            try {
                date = f.parse(hourlyDatum.getDatetime());
                @SuppressLint("SimpleDateFormat") String now = new SimpleDateFormat("h aa").format(date);
                textViewTime.setText(now);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            textViewWeatherTemperature.setText(BindingUtils.convertDegree(hourlyDatum.getAppTemp()));
            textViewWindDirection.setText(hourlyDatum.getWindCdir() + " " + hourlyDatum.getWindSpd().intValue() + " m/s");
            imageViewWeatherIcon.setImageDrawable(BindingUtils.getWeatherIcon(context, hourlyDatum.getWeather().getIcon()));
        }
    }
}
