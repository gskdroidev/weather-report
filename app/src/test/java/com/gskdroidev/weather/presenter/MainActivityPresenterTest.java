package com.gskdroidev.weather.presenter;

import com.gskdroidev.weather.model.interfaces.MainActivityView;

import com.gskdroidev.weather.model.interfaces.WeatherRepository;
import com.gskdroidev.weather.model.pojo.hourly_weather.HourlyWeather;

import org.junit.Test;

import io.reactivex.Single;

public class MainActivityPresenterTest {

    @Test
    public void shouldPassWeather(){
        //Given
        MainActivityView view = new MockMainActivityView();
        WeatherRepository repository = new MockWeatherRepository();

        //When
        MainActivityPresenter presenter = new MainActivityPresenter(view,repository);


        //Then
       // Assert.assertEquals(true,((MockMainActivityView)view).isSuccessCurrentWeather);
      //  Assert.assertEquals(false,((MockMainActivityView)view).isFailureCurrentWeather);
    }

    private class MockMainActivityView implements MainActivityView{

        boolean isSuccessCurrentWeather;
        boolean isFailureCurrentWeather;


        @Override
        public void successHourlyWeather(HourlyWeather hourlyWeather) {

        }

        @Override
        public void failureHourlyWeather(String message) {

        }


    }

    private class MockWeatherRepository implements WeatherRepository{




        @Override
        public Single<HourlyWeather> getHourlyWeather(String city) {
            return null;
        }
    }
}